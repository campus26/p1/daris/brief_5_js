//detruire la div sidebar
let asidediv = document.querySelector('.sidebar');
console.log(asidediv);
let pere = asidediv.closest('div');
pere.remove();
console.log(pere);

//changer de classe de la div class="col-lg-9" en class="col-lg-12" pour aggrandir l'affichage de la div
let vdiv = document.querySelector('.main .container .row div.col-lg-9');
console.log(vdiv); 
vdiv.classList.remove('col-lg-9');
vdiv.classList.add('col-lg-12');

//Modifs be la bordure du bouton subscribe
let btn = document.querySelector('button.btn.btn-primary.text-color-light.text-2.py-3.px-4');
console.log(btn);
btn.style.borderRadius = "0%";

//Modifs du positionnement du form "add to cart"
let form = document.querySelector('form.cart');
let btn2 = document.querySelector('button.btn.btn-primary.btn-modern.text-uppercase')
//console.log(btn2);
form.style.display = "flex";
form.style.flexDirection = "column";
btn2.style.width = "150px"
btn2.style.fontSize = "1.1em";
btn2.style.padding = "17px "
btn2.style.margin = "auto";
let input = document.querySelector('div.quantity.quantity-lg');
input.style.margin = "auto";
input.style.marginBottom = "25px";

//Déplacer le span categories au dessus du form.cart
const cat = document.querySelector(".entry-summary>.product-meta");  //block avec texte categories
form.parentNode.insertBefore(cat, form);
cat.style.marginBottom = "19%";

//Modifier l'image du logo
const logo = document.querySelector('.header-logo a img');
console.log(logo);
logo.src = "./img/logo-corporate-4.png";


